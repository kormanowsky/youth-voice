from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.contrib.auth import authenticate, login, logout
from .models import Post
from .forms import LoginForm


def home_view(request):
    return render(request, "home.html", {
        "posts": Post.objects.order_by("-created")[:5]
    })


def login_view(request):
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        form = LoginForm(request.POST)
        if not form.is_valid():
            # TODO: add django message
            return HttpResponse(status=400)
        user = authenticate(request, **form.cleaned_data)
        if user is None:
            # TODO: add django message
            return HttpResponse(status=401)
        login(request, user)
        return redirect("/")
    else:
        return render(request, "login.html", {})


@login_required
def logout_view(request):
    logout(request)
    return redirect("/")


def post_view(request, post_url_name):
    post = get_object_or_404(Post.objects.all(), url_name=post_url_name)
    return render(request, "post.html", {
        "post": post
    })

