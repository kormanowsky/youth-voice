from django import forms
from .models import User

class LoginForm(forms.Form):

    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
