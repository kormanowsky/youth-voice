from django.apps import AppConfig


class YouthvoicewebappConfig(AppConfig):
    name = 'youthvoicewebapp'
