from django.db import models
from django.contrib.auth.models import User


class Tag(models.Model):

    name = models.CharField(max_length=500)
    url_name = models.CharField(max_length=500)


class Post(models.Model):

    title = models.CharField(max_length=1000)
    content = models.TextField()
    author = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    removed = models.DateTimeField(null=True, blank=True, default=None)
    tags = models.ManyToManyField(to=Tag, blank=True)
    url_name = models.CharField(max_length=500)


